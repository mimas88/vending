<?php

namespace app\controllers;

use app\models\SessionStorage;
use app\models\Transaction;
use app\models\Wallet;
use Yii;
use yii\data\ArrayDataProvider;
use yii\web\Controller;
use yii\web\Response;


class SiteController extends Controller
{

  public function init()
  {
    parent::init();
  }


  /**
   * {@inheritdoc}
   */
  public function actions()
  {
    return [
        'error' => [
            'class' => 'yii\web\ErrorAction',
        ],
    ];
  }


  /**
   * Displays homepage.
   *
   * @return string
   */
  public function actionIndex()
  {
    $wallet = new \app\models\CashBridge(new \app\models\Wallet(), new SessionStorage());
    $cashBox = new \app\models\CashBridge(new \app\models\CashBox(), new SessionStorage());
    $deposit = new \app\models\Deposit(new SessionStorage());
    $showcase = new \app\models\Showcase(new SessionStorage());

    return $this->render('index', [
        'wallet'   => $wallet,
        'cashBox'  => $cashBox,
        'deposit'  => $deposit,
        'showcase' => $showcase,
    ]);
  }


  public function actionInsertCoin()
  {
    $wallet = new \app\models\CashBridge(new \app\models\Wallet(), new SessionStorage());
    $cashBox = new \app\models\CashBridge(new \app\models\CashBox(), new SessionStorage());
    $deposit = new \app\models\Deposit(new SessionStorage());
    $par = Yii::$app->getRequest()->get('par');
    $count = Yii::$app->getRequest()->get('count');
    if(!empty($par)) {
      if(empty($count)) {
        $count = 1;
      }
      $transaction = new Transaction();
      $transaction->addObject($wallet, 'commitTransaction', 'rollbackTransaction');
      $transaction->addObject($cashBox, 'commitTransaction', 'rollbackTransaction');
      $transaction->addObject($deposit, 'commitTransaction', 'rollbackTransaction');
      try {
        $wallet->removeItem($par, $count);
        $cashBox->addItem($par, $count);
        $deposit->addAmount(\app\models\CashModel::getAmountByPar($par, $count));
        $transaction->commit();
      } catch(\Exception $e) {
        $transaction->rollback();

        return $this->asJson(['error' => [
            'message' => $e->getMessage(),
            'code'    => $e->getCode(),
        ],
                             ]);
      }
    }

    $data = [
        'message' => '',
        'wallet'  => [
            'pars' => $wallet->getPars(),
        ],
        'cashBox' => [
            'pars' => $cashBox->getPars(),
        ],
        'deposit' => [
            'amount' => $deposit->getAmount(),
        ],
    ];

    return $this->asJson($data);
  }


  public function actionCashBack()
  {
    $wallet = new \app\models\CashBridge(new \app\models\Wallet(), new SessionStorage());
    $cashBox = new \app\models\CashBridge(new \app\models\CashBox(), new SessionStorage());
    $deposit = new \app\models\Deposit(new SessionStorage());

    $depositAmount = $deposit->getAmount();
    if($depositAmount > 0) {
      $transaction = new Transaction();
      $transaction->addObject($cashBox, 'commitTransaction', 'rollbackTransaction');
      $transaction->addObject($wallet, 'commitTransaction', 'rollbackTransaction');
      $transaction->addObject($deposit, 'commitTransaction', 'rollbackTransaction');
      try {
        $depositAmount = $deposit->getAmount();
        $cashPars = $cashBox->doCashBack($depositAmount);
        $deposit->setAmount(0);
        foreach($cashPars as $par => $count) {
          $wallet->addItem($par, $count);
        }

        $transaction->commit();
      } catch(\Exception $e) {
        $transaction->rollback();

        return $this->asJson(['error' => [
            'message' => $e->getMessage(),
            'code'    => $e->getCode(),
        ],
                             ]);
      }
    }

    $data = [
        'message' => '',
        'wallet'  => [
            'pars' => $wallet->getPars(),
        ],
        'cashBox' => [
            'pars' => $cashBox->getPars(),
        ],
        'deposit' => [
            'amount' => $deposit->getAmount(),
        ],
    ];

    return $this->asJson($data);
  }


  public function actionProductBuy()
  {
    $outputMessage = '';
    $deposit = new \app\models\Deposit(new SessionStorage());
    $showcase = new \app\models\Showcase(new SessionStorage());

    $id = Yii::$app->getRequest()->get('id');
    $count = Yii::$app->getRequest()->get('count');
    if(!empty($id)) {
      if(empty($count)) {
        $count = 1;
      }
      $transaction = new Transaction();
      $transaction->addObject($deposit, 'commitTransaction', 'rollbackTransaction');
      $transaction->addObject($showcase, 'commitTransaction', 'rollbackTransaction');
      try {

        $totalSum = $showcase->getAmountItem($id, $count);
        $deposit->removeAmount($totalSum);
        $showcase->removeItem($id, $count);

        $transaction->commit();

        $outputMessage = 'Спасибо!';
      } catch(\Exception $e) {
        $transaction->rollback();

        return $this->asJson(['error' => [
            'message' => $e->getMessage(),
            'code'    => $e->getCode(),
        ],
                             ]);
      }
    }
    $data = [
        'message'  => $outputMessage,
        'deposit'  => [
            'amount' => $deposit->getAmount(),
        ],
        'showcase' => [
            'items' => $showcase->getItems(),
        ],
    ];

    return $this->asJson($data);
  }


  public function actionResetData()
  {
    $wallet = new \app\models\CashBridge(new \app\models\Wallet(), new SessionStorage());
    $cashBox = new \app\models\CashBridge(new \app\models\CashBox(), new SessionStorage());
    $deposit = new \app\models\Deposit(new SessionStorage());
    $showcase = new \app\models\Showcase(new SessionStorage());

    $wallet->resetToDefaultData();
    $cashBox->resetToDefaultData();
    $deposit->resetToDefaultData();
    $showcase->resetToDefaultData();

    $data = [
        'message'  => 'Все данные сброшены до значений по умолчанию',
        'wallet'   => [
            'pars' => $wallet->getPars(),
        ],
        'cashBox'  => [
            'pars' => $cashBox->getPars(),
        ],
        'deposit'  => [
            'amount' => $deposit->getAmount(),
        ],
        'showcase' => [
            'items' => $showcase->getItems(),
        ],
    ];

    return $this->asJson($data);
  }


}
