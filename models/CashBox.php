<?php

namespace app\models;

use yii\helpers\ArrayHelper;


class CashBox extends CashModel
{
  public $defaultCashData = [
      self::PAR_ONE  => 100,
      self::PAR_TWO  => 100,
      self::PAR_FIVE => 100,
      self::PAR_TEN  => 100,
  ];

  const STORAGE_KEY = 'cashBoxData';


  /**
   *
   *
   * @param $amount
   * @return array|bool
   * @throws \Exception
   */
  public function doCashBack($amount)
  {
    $amount = (int)$amount;
    if($this->getAmount() < $amount) {
      throw new \Exception('В кассе недостаточно средств.');
    }
    $cashPars = $this->getPars();
    krsort($cashPars);
    $result = $this->generateMinPars($cashPars,$amount);

    if($result === false) {
      throw new \Exception('В кассе не хватает нужных момент.');
    }

    foreach($result as $par => $count) {
      $this->removeItem($par, $count);
    }

    return $result;
  }


  /**
   *
   *
   * @param $cashPars
   * @param $amount
   * @return array|bool
   */
  protected function generateMinPars($cashPars, $amount)
  {
    $result = [];
    $tmpPars = $cashPars;
    foreach($cashPars as $par => $issetCount) {
      if($amount >= $par) {
        $countNeeded = (int)($amount / $par);
        if($countNeeded <= $issetCount) {
          $divisionRemainder = $amount % $par;
          $amount = $divisionRemainder;
          $result[$par] = $countNeeded;
        }
        else {
          $amount = $amount - ($issetCount * $par);
          if(!empty($issetCount)) {
            $result[$par] = $issetCount;
          }
        }
        unset($tmpPars[$par]);
        $tmpResult = $this->generateMinPars($tmpPars, $amount);
        // нельзя набрать достаточное количество
        if($tmpResult === false) {
          // берём вернюю монету, попробуем добить из оставшихся
          if(!empty($result[$par])) {
            $result[$par]--;
            $amount += $par;
          }
        }
        else {
          // добрали минимальное количество
          $result = ArrayHelper::merge($result, $tmpResult);
          $amount = 0;
          break;
        }
      }
    }

    if($amount != 0) {
      return false;
    }

    return $result;
  }


}