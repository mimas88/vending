<?php

namespace app\models;

class Transaction
{

  protected $poolObjects = [];
  protected $backupObjects;


  public function addObject($object, $commitFunction, $rollbackFunction)
  {
    /* @var $object \app\models\ITransaction*/
    $object->setIsTransaction(true);

    $this->poolObjects[] = [
        'object'           => $object,
        'commitFunction'   => $commitFunction,
        'rollbackFunction' => $rollbackFunction,
    ];
  }


  protected function unsetPoolObjects()
  {
    $this->poolObjects = [];
  }


  public function commit()
  {
    foreach($this->poolObjects as $objectData) {
      /* @var $object \app\models\ITransaction */
      $object = $objectData['object'];
      $this->callFunction($object, $objectData['commitFunction']);
      $object->setIsTransaction(false);
    }
  }


  public function rollback()
  {
    foreach($this->poolObjects as $objectData) {
      /* @var $object \app\models\ITransaction */
      $object = $objectData['object'];
      $this->callFunction($object, $objectData['rollbackFunction']);
      $object->setIsTransaction(false);
    }
  }


  protected function callFunction($object, $function)
  {
    if(is_callable([$object, $function])) {
      call_user_func([$object, $function]);
    }
  }
}