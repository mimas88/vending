<?php

namespace app\models;

/**
 * @property \app\models\CashBridge | null $copyThisObject
 * @author  Mimas
 * @package app\models
 */
class CashBridge implements ITransaction
{

  protected $model;
  protected $storage;
  protected $storageKey;
  protected $isTransaction = false;
  protected $copyThisObject;


  public function __construct(CashModel $model, IStorage $storage)
  {
    $this->model = $model;
    $this->storage = $storage;
    $this->storageKey = $model::STORAGE_KEY;

    $this->loadData();
  }



  protected function loadData()
  {
    $cashData = $storageData = $this->storage->getData($this->storageKey);
    if(empty($cashData)) {
      $cashData = $this->model->defaultCashData;
    }
    foreach($cashData as $par => $count) {
      $this->model->setParCount($par, $count);
    }
    if(empty($storageData)) {
      $this->updateStorage();
    }
  }


  public function __call($name, $arguments)
  {
    $result = null;
    $methodName = [$this->model, $name];
    if(is_callable($methodName)) {
      $result = call_user_func_array($methodName, $arguments);
      if(!$this->isTransaction) {
        $this->updateStorage();
      }
    }

    return $result;
  }


  public function updateStorage()
  {
    $this->storage->setData($this->storageKey, $this->model->getDataForStorage());
  }

  public function resetToDefaultData()
  {
    $this->model->unsetItems();
    $this->updateStorage();
    $this->loadData();
  }

  public function setIsTransaction($isTransaction)
  {
    if($isTransaction) {
      $this->copyThisObject = clone $this;
    }
    $this->isTransaction = $isTransaction;
  }

  public function commitTransaction()
  {
    $this->updateStorage();
  }

  public function rollbackTransaction()
  {
    // set old data to storage
    $this->copyThisObject->updateStorage();
    // clear new data
    $this->model->unsetItems();
    // get old data from storage
    $this->loadData();
  }


}