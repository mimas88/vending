<?php

namespace app\models;


abstract class CashModel
{
  public $defaultCashData;

  protected $monies = [];

  protected static $allowedPar = [
      self::PAR_ONE  => 1,
      self::PAR_TWO  => 2,
      self::PAR_FIVE => 5,
      self::PAR_TEN  => 10,
  ];

  const PAR_ONE = 1;
  const PAR_TWO = 2;
  const PAR_FIVE = 5;
  const PAR_TEN = 10;

  const STORAGE_KEY = 'cashModelData';

  public function getPars($par = '')
  {

    return !empty($par) ? $this->monies[$par] : $this->monies;
  }

  public function setParCount($par,$count)
  {
    $par = (int)$par;
    $count = (int)$count;
    if(self::isAllowedPar($par)) {
      $this->monies[$par] = $count;
    }
  }


  public function getDataForStorage()
  {
    return $this->getPars();
  }


  public function getAmount()
  {
    $result = 0;
    foreach($this->monies as $par => $count) {
      $result += self::getAmountByPar($par,$count);
    }
    return $result;
  }


  /**
   *
   *
   * @param int $par
   * @param int $count
   * @return bool
   * @throws \Exception
   */
  public function removeItem($par, $count)
  {
    if(!$this->checkAvailableItem($par, $count)) {
      throw new \Exception('Не достаточно монет номиналом ' . $par . ' ' . \Yii::$app->params['currencySymbol']);
    }

    $this->monies[$par] = $this->monies[$par] - $count;

    return true;
  }


  protected function checkAvailableItem($par, $count)
  {
    if($this->monies[$par] >= $count) {
      return true;
    }

    return false;
  }


  public function addItem($par, $count)
  {
    $par = (int)$par;
    $count = (int)$count;
    if(self::isAllowedPar($par)) {
      $this->monies[$par] += $count;
    }
  }

  public function unsetItems()
  {
    $this->monies = [];
  }


  public static function isAllowedPar($par)
  {
    return array_key_exists($par, self::$allowedPar);
  }


  public static function getAmountByPar($par, $count)
  {
    return $par * $count;
  }
}