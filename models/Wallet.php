<?php

namespace app\models;

class Wallet extends CashModel
{
  public $defaultCashData = [
      self::PAR_ONE  => 10,
      self::PAR_TWO  => 30,
      self::PAR_FIVE => 20,
      self::PAR_TEN  => 15,
  ];

  const STORAGE_KEY = 'walletData';
}