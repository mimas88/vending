<?php

namespace app\models;

/**
 *
 * @property \app\models\Showcase | null $copyThisObject
 * @author  Mimas
 * @package app\models
 */
class Showcase implements ITransaction
{
  protected $items = [];

  protected $storage;
  protected $isTransaction = false;
  protected $copyThisObject;

  const STORAGE_KEY = 'showcase';


  public function __construct(IStorage $storage)
  {
    $this->storage = $storage;
    $this->loadData();
  }


  protected function loadData()
  {
    $showcaseData = $storageData = $this->storage->getData(self::STORAGE_KEY);
    if(empty($showcaseData)) {
      $showcaseData[Product::PRODUCT_TEA] = 10;
      $showcaseData[Product::PRODUCT_COFFEE] = 20;
      $showcaseData[Product::PRODUCT_COFFEE_WITH_MILK] = 20;
      $showcaseData[Product::PRODUCT_JUICE] = 15;
    }

    foreach($showcaseData as $id => $count) {
      $this->setItemCount($id, $count);
    }
    if(empty($storageData)) {
      $this->updateStorage();
    }
  }


  /**
   *
   *
   * @param $id
   * @param $count
   * @return bool
   * @throws \Exception
   */
  public function removeItem($id, $count)
  {
    $id = (int)$id;
    $count = (int)$count;
    if(!$this->checkAvailableItem($id, $count)) {
      throw new \Exception(Product::getProductName($id) . ' закончился...');
    }

    $this->items[$id] = $this->items[$id] - $count;

    return true;
  }


  public function checkAvailableItem($id, $count)
  {
    if($this->items[$id] >= $count) {
      return true;
    }

    return false;
  }


  public function getAmountItem($id, $count)
  {
    $id = (int)$id;
    $count = (int)$count;
    return (Product::getProductPrice($id) * $count);
  }


  protected function setItemCount($id, $count)
  {
    $id = (int)$id;
    $count = (int)$count;
    if(Product::isAllowedProduct($id)) {
      $this->items[$id] = $count;
    }
  }

  protected function unsetItems()
  {
    $this->items = [];
  }


  public function getItems()
  {
    return $this->items;
  }


  public function updateStorage()
  {
    $this->storage->setData(self::STORAGE_KEY, $this->items);
  }


  public function resetToDefaultData()
  {
    $this->unsetItems();
    $this->updateStorage();
    $this->loadData();
  }



  public function setIsTransaction($isTransaction)
  {
    if($isTransaction) {
      $this->copyThisObject = clone $this;
    }
    $this->isTransaction = $isTransaction;
  }


  public function commitTransaction()
  {
    $this->updateStorage();
  }


  public function rollbackTransaction()
  {
    // set old data to storage
    $this->copyThisObject->updateStorage();
    // clear new data
    $this->unsetItems();
    // get old data from storage
    $this->loadData();
  }

}