<?php

namespace app\models;

interface ITransaction {

  public function setIsTransaction($isTransaction);
  public function commitTransaction();
  public function rollbackTransaction();
}
 