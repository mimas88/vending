<?php

namespace app\models;

/**
 * @author  Mimas
 * @package app\models
 */
class Product
{
  protected static $productInfo = [
      self::PRODUCT_TEA              => [
          'id'    => self::PRODUCT_TEA,
          'name'  => 'Чай',
          'price' => 13,
      ],
      self::PRODUCT_COFFEE           => [
          'id'    => self::PRODUCT_COFFEE,
          'name'  => 'Кофе',
          'price' => 18,
      ],
      self::PRODUCT_COFFEE_WITH_MILK => [
          'id'    => self::PRODUCT_COFFEE_WITH_MILK,
          'name'  => 'Кофе с молоком',
          'price' => 21,
      ],
      self::PRODUCT_JUICE            => [
          'id'    => self::PRODUCT_JUICE,
          'name'  => 'Сок',
          'price' => 35,
      ],
  ];

  const PRODUCT_TEA = 1;
  const PRODUCT_COFFEE = 2;
  const PRODUCT_COFFEE_WITH_MILK = 3;
  const PRODUCT_JUICE = 4;


  public static function isAllowedProduct($id)
  {
    return array_key_exists($id, self::$productInfo);
  }


  /**
   *
   *
   * @param $id
   * @return array|null
   */
  public static function getProduct($id)
  {
    return array_key_exists($id,self::$productInfo) ? self::$productInfo[$id] : null;
  }


  /**
   *
   *
   * @param $id
   * @return string
   */
  public static function getProductName($id)
  {
    $product = self::getProduct($id);

    return !empty($product) ? $product['name'] : 'отсутствует имя';
  }


  /**
   *
   *
   * @param $id
   * @return string
   */
  public static function getProductPrice($id)
  {
    $product = self::getProduct($id);

    return !empty($product) ? $product['price'] : false;
  }
  /**
   *
   *
   * @param $id
   * @return string
   */
  public static function getProductDescription($id)
  {
    $product = self::getProduct($id);
    return !empty($product) ? $product['name'] . ' - ' . $product['price'] . ' '.\Yii::$app->params['currencySymbol'] : 'отсутствует описание';
  }
}