<?php

namespace app\models;

class SessionStorage implements IStorage
{
  protected $storage;
  const STORAGE_SESSION = 'session';
  const STORAGE_ARRAY = 'array';


  public function __construct($storageType = self::STORAGE_SESSION)
  {
    switch($storageType) {
      case self::STORAGE_ARRAY:
        $this->storage = [];
        break;
      case self::STORAGE_SESSION:
      default:
        $this->storage = \Yii::$app->getSession();
        break;
    }
  }


  public function getData($key)
  {
    return isset($this->storage[$key]) ? $this->storage[$key] : null;
  }

  public function setData($key,$data)
  {
    $this->storage[$key] = $data;
  }
}
 