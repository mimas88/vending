<?php

namespace app\models;

interface IStorage
{
  public function getData($key);


  public function setData($key,$data);
}