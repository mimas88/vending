<?php

namespace app\models;

/**
 * @property \app\models\Deposit | null $copyThisObject
 * @author  Mimas
 * @package app\models
 */
class Deposit implements ITransaction
{
  protected $amount;
  protected $storage;
  protected $isTransaction = false;
  protected $copyThisObject;

  const STORAGE_KEY = 'deposit';
  const AMOUNT_DEFAULT = 0;


  public function __construct(IStorage $storage)
  {
    $this->storage = $storage;
    $this->loadData();
  }


  protected function loadData()
  {
    $amountData = $this->storage->getData(self::STORAGE_KEY);
    if(empty($amountData)) {
      $amountData = self::AMOUNT_DEFAULT;
    }
    $this->setAmount($amountData);
  }


  public function updateStorage()
  {
    $this->storage->setData(self::STORAGE_KEY, $this->amount);
  }

  public function resetToDefaultData()
  {
    $this->setAmount(0);
    $this->updateStorage();
    $this->loadData();
  }

  public function getAmount()
  {
    return $this->amount;
  }


  public function addAmount($amount)
  {
    $amount = (int)$amount;
    $this->amount += $amount;
  }


  public function removeAmount($amount)
  {
    $amount = (int)$amount;
    if($this->getAmount() < $amount) {
      throw new \Exception('Недостаточно средств.');
    }
    $this->amount -= $amount;
  }


  public function setAmount($amount)
  {
    $this->amount = (int)$amount;
  }


  public function setIsTransaction($isTransaction)
  {
    if($isTransaction) {
      $this->copyThisObject = clone $this;
    }
    $this->isTransaction = $isTransaction;
  }


  public function commitTransaction()
  {
    $this->updateStorage();
  }


  public function rollbackTransaction()
  {
    // set old data to storage
    $this->copyThisObject->updateStorage();
    // get old data from storage
    $this->loadData();
  }


}