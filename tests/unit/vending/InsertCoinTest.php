<?php

namespace tests\vending;

use app\models\CashModel;


/**
 *
 * @property \app\models\CashModel $wallet
 * @property \app\models\Deposit $deposit
 * @author  Mimas
 * @package tests\vending
 */
class InsertCoinTest extends \Codeception\Test\Unit
{
  protected function setUp()
  {
    $this->wallet = new \app\models\CashBridge(new \app\models\Wallet(), new \app\models\SessionStorage(\app\models\SessionStorage::STORAGE_ARRAY));
    $this->deposit = new \app\models\Deposit(new \app\models\SessionStorage(\app\models\SessionStorage::STORAGE_ARRAY));
  }


  protected function tearDown()
  {
    $this->wallet = null;
    $this->deposit = null;
  }


  /**
   * @dataProvider providerInsertCoin
   * @param int $par
   * @param int $walletSum
   * @param int $balance
   * @throws \Exception
   */
  public function testInsertCoin($par, $walletSum, $balance)
  {
    $this->wallet->removeItem($par, 1);
    $this->assertEquals(0, $this->deposit->getAmount());
    $this->deposit->addAmount(CashModel::getAmountByPar($par, 1));
    $this->assertEquals($walletSum, $this->wallet->getAmount());
    $this->assertEquals($balance, $this->deposit->getAmount());
  }


  public function providerInsertCoin()
  {
    $data = [
        [1,319,1],
        [2,318,2],
        [5,315,5],
        [10,310,10],
    ];


    return $data;
  }
}