<?php

/* @var $this yii\web\View */
/* @var $wallet \app\models\CashBridge */
/* @var $cashBox \app\models\CashBridge */
/* @var $deposit \app\models\Deposit */
/* @var $showcase \app\models\Showcase */

$this->title = 'Vending machine';
?>
<div class="site-index">
  <div id="vending">
    <img src="images/vending.png">
    <div id="vending_products" class="js_showcase_root">
      <ul>
        <?php
        $productPositions = $showcase->getItems();
        if(!empty($productPositions)) {
          foreach($productPositions as $productId => $productCount) {
            $productIsEnded = $productCount == 0 ? true : false;
            ?>
            <li>
              <p>
                <button class="btn btn-sm btn-success<?php if($productIsEnded) {echo " hidden";} ?> js_product-buy" data-count="<?php echo $productCount; ?>"
                        data-id="<?php echo $productId; ?>">купить
                </button>
                <span class="js_product-empty-text<?php if(!$productIsEnded) {echo " hidden";}?>">Нет в наличии</span>
                <span class="product-description"><?php echo \app\models\Product::getProductDescription($productId) ?></span>
                (<span class="js_product-count"><?php echo $productCount; ?></span>шт.)
              </p>
            </li>
            <?php
          }
        }
        ?>
      </ul>
    </div>
    <div id="vending_money" class="js_cash-box_root">
      <ul>
        <?php
        $cashBoxMonies = $cashBox->getPars();

        if(!empty($cashBoxMonies)) {
          foreach($cashBoxMonies as $par => $count) {
            ?>
            <li class="js_cash-box_item" data-par="<?php echo $par; ?>">
              <p><?php echo $par . ' ' . Yii::$app->params['currencySymbol']; ?> (<span class="js_cash-box_count"><?php echo $count ?></span>шт.)</p>
            </li>
            <?php
          }
        }
        ?>
      </ul>
    </div>
    <div id="vending_deposit" class="js_deposit_root">
      <p id="vending_deposit_title">Внесено:</p>
      <p id="vending_deposit_value">
        <span class="js_deposit_amount"><?php echo $deposit->getAmount() ?></span>&nbsp;<?php echo Yii::$app->params['currencySymbol']; ?>
      </p>
      <button class="btn btn-sm btn-success js_deposit_cash-back" <?php if($deposit->getAmount() == 0) {
        echo 'disabled="disabled"';
      } ?>>сдача
      </button>
    </div>
  </div>
  <div id="wallet" class="js_wallet_root">
    <img src="images/wallet.png">
    <ul>
      <?php
      $walletMonies = $wallet->getPars();

      if(!empty($walletMonies)) {
        foreach($walletMonies as $par => $count) {
          ?>
          <li>
            <p>
              <button class="btn btn-sm btn-success js_wallet_insert-coin" <?php if($count == 0) {
                echo 'disabled="disabled"';
              } ?> data-par="<?php echo $par; ?>">внести монету
              </button>&nbsp;&nbsp;&nbsp;<?php echo $par . ' ' . Yii::$app->params['currencySymbol']; ?> (<span class="js_wallet_count"><?php echo $count ?></span>шт.)

            </p>
          </li>
          <?php
        }
      }
      ?>
    </ul>
  </div>
  <div id="reset-button">
    <a href="javascript:;" class="js_reset-button"><img src="images/reset-button.png"></a>
  </div>
</div>