// хелпер для служебных функций, работы со строками, массивами и т.д.
var HelperFunctions = new function() {
  var _this = this;

  /**
   * Экранировать символы в строке.
   *
   * @param str - строка, в которой делается замена
   * @param charsArr - массив символов, которые нужно экранировать
   * @param escapingChar - символ, который будет экранировать. При нахождении в исходной строке
   * будет экранировать сам себя. Обратный слэш по дефолту.
   * @returns new string.
   */
  this.escapeChars = function(str, charsArr, escapingChar) {
    var escapingChar = escapingChar || '\\',
        pattern, symbol, result;
    if(escapingChar == '\\') {
      pattern = new RegExp('\\\\');
    } else {
      pattern = new RegExp(escapingChar, 'g');
    }
    result = str.replace(pattern, escapingChar + escapingChar);
    for(var i = 0; i < charsArr.length; i++) {
      symbol = charsArr[i];
      if(symbol === escapingChar) {
        continue;
      }
      pattern = new RegExp(symbol, 'g');
      result = result.replace(pattern, escapingChar + symbol);
    }
    return result;
  };

  // показывает элемент
  this.showItem = function($el) {
    if($el.hasClass('hidden')) {
      $el.removeClass('hidden');
    }
  };

  // скрывает элемент
  this.hideItem = function($el) {
    if(!$el.hasClass('hidden')) {
      $el.addClass('hidden');
    }
  };

  // проверяет скрыт ли элемент
  this.isHiddenItem = function($el) {
    return $el.hasClass('hidden');
  };

  // проверяет скрыт ли элемент
  this.addClass = function($el, className) {
    if(!$el.hasClass(className)) {
      $el.addClass(className);
    }
  };

  /**
   * Прокрутка до элемента
   * @param position string|object|integer
   */
  this.scrollTo = function(position) {
    if(typeof position === 'string') {
      position = $(position);
    }
    if(typeof position === 'object' && position.length > 0) {
      position = position.offset().top;
    }
    $("html, body").animate({scrollTop: position}, "slow")
  };

  // проверяет пустая ли строка
  this.isEmptyString = function(str) {
    return !!(typeof str === 'undefined' || str.trim().length === 0);
  };

  /**
   Устанавливает флаг последнего ajax запроса и возвращает флаг текущего запроса
   При success реализовывается логика: если флаги не равны, то ничего не происходит, ждём ответ на следующий запрос
   */
  this.setAjaxRequestFlag = function(object, propertyName) {
    var currentAjaxRequestFlag = new Date().getTime();
    Object.defineProperty(object, propertyName, {value: currentAjaxRequestFlag, configurable: true, writable: true, enumerable: true});
    return currentAjaxRequestFlag;
  };

  // возвращает объект полей и значеий формы
  this.getFormValues = function($form) {
    return $form.serializeArray().reduce(function(obj, item) {
      obj[item.name] = item.value;
      return obj;
    }, {});
  };

  /** строит url
   Example:
   HelperFunctions.buildUrl('http://example.com', {
   path: 'about',
   hash: 'contact',
   queryParams: {
   foo: bar,
   bar: ['foo', 'bar']
   });
   */
  this.buildUrl = function(url, options) {
    var queryString = [];
    var key;
    var builtUrl;

    if(url === null) {
      builtUrl = '';
    } else if(typeof(url) === 'object') {
      builtUrl = '';
      options = url;
    } else {
      builtUrl = url;
    }

    if(options) {
      if(options.path) {
        if(options.path.indexOf('/') === 0) {
          builtUrl += options.path;
        } else {
          builtUrl += '/' + options.path;
        }
      }

      if(options.queryParams) {
        for(key in options.queryParams) {
          if(options.queryParams.hasOwnProperty(key)) {
            queryString.push(key + '=' + options.queryParams[key]);
          }
        }
        builtUrl += '?' + queryString.join('&');
      }

      if(options.hash) {
        builtUrl += '#' + options.hash;
      }
    }

    return builtUrl;
  };

  // возвращает объект параметров get-запроса
  this.getRequestParams = function() {
    return window.location.search.replace('?', '').split('&')
      .reduce(
        function(p, e) {
          if(e.length > 0) {
            var a = e.split('=');
            p[decodeURIComponent(a[0])] = decodeURIComponent(a[1]);
          }
          return p;
        }, {}
      );
  };

  // возвращает строку из объекта
  this.objectToString = function(object, glue, separator) {
    if(glue == undefined) {
      glue = '=';
    }

    if(separator == undefined) {
      separator = ',';
    }

    return $.map(Object.getOwnPropertyNames(object), function(k) {
      return [k, object[k]].join(glue)
    }).join(separator);
  };

  // устанавливает счётчик Яндекс.метрики
  this.sendAnalytics = function(service) {
    if('ya' in service && typeof yaCounter !== 'undefined') {
      console.log('send yandex ' + service['ya']['name']);
      yaCounter.reachGoal(service['ya']['name']);
    }
    if('ga' in service && typeof ga !== 'undefined') {
      var func   = 'func' in service['ga'] ? service['ga']['func'] : 'send',
          event  = 'event' in service['ga'] ? service['ga']['event'] : 'event',
          target = 'target' in service['ga'] ? service['ga']['target'] : 'button',
          action = 'action' in service['ga'] ? service['ga']['action'] : 'click',
          name   = 'name' in service['ga'] ? service['ga']['name'] : 'emptyName';
      console.log('send google ' + name);
      ga(func, event, target, action, name);
    }
  }
};
