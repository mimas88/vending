var Vending = new function() {
  var _this = this;

  this.ajaxUrls = {
    'buy':        'product-buy',
    'cashBack':   'cash-back',
    'insertCoin': 'insert-coin',
    'resetData': 'reset-data'
  }
  this.selectors = {
    'showcaseRoot':           '.js_showcase_root',
    'showcaseBuyButton':      '.js_product-buy',
    'showcaseItemEmptyText':  '.js_product-empty-text',
    'showcaseItemCount':      '.js_product-count',
    'cashBoxRoot':            '.js_cash-box_root',
    'cashBoxPar':             '.js_cash-box_item',
    'cashBoxParCount':        '.js_cash-box_count',
    'depositRoot':            '.js_deposit_root',
    'depositAmount':          '.js_deposit_amount',
    'depositCashBackButton':  '.js_deposit_cash-back',
    'walletRoot':             '.js_wallet_root',
    'walletInsertCoinButton': '.js_wallet_insert-coin',
    'walletItemCount':        '.js_wallet_count',
    'resetButton':        '.js_reset-button',
  };

  this.init = function() {
    _this.$showcase = $(_this.selectors['showcaseRoot']);
    _this.$deposit = $(_this.selectors['depositRoot']);
    _this.$cashBox = $(_this.selectors['cashBoxRoot']);
    _this.$wallet = $(_this.selectors['walletRoot']);
    _this.$resetButton = $(_this.selectors['resetButton']);
    _this.$showcase.on('click', _this.selectors['showcaseBuyButton'], _this.actionBuy);
    _this.$deposit.on('click', _this.selectors['depositCashBackButton'], _this.actionCashBack);
    _this.$wallet.on('click', _this.selectors['walletInsertCoinButton'], _this.actionInsertCoin);
    _this.$resetButton.on('click', _this.actionResetData);
  };

  this.actionBuy = function() {
    _this.sendRequest(_this.ajaxUrls['buy'], {'id': $(this).data('id')});
  };

  this.actionCashBack = function() {
    _this.sendRequest(_this.ajaxUrls['cashBack']);
  };

  this.actionInsertCoin = function() {
    _this.sendRequest(_this.ajaxUrls['insertCoin'], {'par': $(this).data('par')});
  };

  this.actionResetData = function() {
    _this.sendRequest(_this.ajaxUrls['resetData']);
  };

  this.afterAjaxSuccess = function(response) {
    if(typeof response['showcase'] !== "undefined") {
      _this.updateShowcaseView(response['showcase']);
    }
    if(typeof response['wallet'] !== "undefined") {
      _this.updateWalletView(response['wallet']);
    }
    if(typeof response['cashBox'] !== "undefined") {
      _this.updateCashBoxView(response['cashBox']);
    }
    if(typeof response['deposit'] !== "undefined") {
      _this.updateDepositView(response['deposit']);
    }
    if(typeof response['message'] !== "undefined" && response['message'] !== '') {
      alert(response['message']);
    }
  }

  this.afterAjaxError = function(response) {
    if(typeof response['error'] !== "undefined" && response['error']['message'] !== "undefined") {
      alert(response['error']['message']);
    }
    else {
      console.log(response);
    }
  }

  this.updateShowcaseView = function(showcaseData) {
    var showcaseItems = showcaseData['items'];
    for(var item in showcaseItems) {
      var $buyButton = _this.$showcase.find(_this.selectors['showcaseBuyButton'] + '[data-id="' + item + '"]'),
          $countText = $buyButton.siblings(_this.selectors['showcaseItemCount']),
          $emptyText = $buyButton.siblings(_this.selectors['showcaseItemEmptyText']),
          itemCount  = showcaseItems[item];
      $buyButton.data('count', itemCount);
      $countText.html(itemCount);
      if(itemCount > 0) {
        HelperFunctions.showItem($buyButton);
        HelperFunctions.hideItem($emptyText);
      }
      else {
        HelperFunctions.showItem($emptyText);
        HelperFunctions.hideItem($buyButton);
      }
    }
  };

  this.updateDepositView = function(deposit) {
    var depositAmount          = deposit['amount'],
        $depositText           = _this.$deposit.find(_this.selectors['depositAmount']),
        $depositCashBackButton = _this.$deposit.find(_this.selectors['depositCashBackButton']);
    $depositText.html(depositAmount);
    if(depositAmount > 0) {
      $depositCashBackButton.removeAttr('disabled');
    }
    else {
      if(!$depositCashBackButton[0].hasAttribute('disabled')) {
        $depositCashBackButton.attr('disabled', 'disabled');
      }
    }

  };

  this.updateCashBoxView = function(cashBox) {
    var cashBoxPars = cashBox['pars'];
    for(var par in cashBoxPars) {
      var $cashBoxPar = _this.$cashBox.find(_this.selectors['cashBoxPar'] + '[data-par="' + par + '"]'),
          $countText  = $cashBoxPar.find(_this.selectors['cashBoxParCount']),
          parCount    = cashBoxPars[par];
      $countText.html(parCount);
    }

  };

  this.updateWalletView = function(wallet) {
    var walletPars = wallet['pars'];
    for(var par in walletPars) {
      var $walletInsertCoinButton = _this.$wallet.find(_this.selectors['walletInsertCoinButton'] + '[data-par="' + par + '"]'),
          $countText              = $walletInsertCoinButton.siblings(_this.selectors['walletItemCount']),
          parCount                = walletPars[par];
      $countText.html(parCount);
      if(parCount > 0) {
        $walletInsertCoinButton.removeAttr('disabled');
      }
      else {
        if(!$walletInsertCoinButton[0].hasAttribute('disabled')) {
          $walletInsertCoinButton.attr('disabled', 'disabled');
        }
      }
    }

  };

  this.sendRequest = function(url, params, successCallback, errorCallback) {
    if(typeof params === "undefined") {
      params = {};
    }
    if(typeof successCallback === "undefined") {
      successCallback = _this.afterAjaxSuccess;
    }
    if(typeof errorCallback === "undefined") {
      errorCallback = _this.afterAjaxError;
    }
    $.ajax({
             type:    'get',
             url:     url,
             data:    params,
             success: function(response) {
               if(typeof response['error'] !== "undefined") {
                 if(typeof errorCallback !== "undefined") {
                   errorCallback(response);
                 }
                 else {
                   console.log(response['error']['message']);
                 }
               }
               else {
                 if(typeof successCallback !== "undefined") {
                   successCallback(response);
                 }
               }
             },
             error:   function(jqXHR) {
               if(typeof errorCallback !== "undefined") {
                 errorCallback(jqXHR);
               }
             }
           });
  }
};


$(document).ready(function() {
  if(typeof Vending !== "undefined") {
    Vending.init();
  }
});